### DOCKER FILE FOR eos-fuse IMAGE ###

#
# Build instructions:
#
# export RELEASE_VERSION="v0"
# docker build -t gitlab-registry.cern.ch/sciencebox/docker-images/eosxd:${RELEASE_VERSION} .
# docker login gitlab-registry.cern.ch
# docker push gitlab-registry.cern.ch/sciencebox/docker-images/eosxd:${RELEASE_VERSION}
#
#
# - If in the need to specify EOSXD version, proceed as follows:
# export EOSXD_VERSION="4.7.3"
# docker build -t gitlab-registry.cern.ch/sciencebox/docker-images/eosxd:${RELEASE_VERSION} --build-arg EOSXD_VERSION="-${EOSXD_VERSION}" .
#

# TODO: Check what follows
#
# NOTE: The container needs SYS_ADMIN capabilities (--cap-add SYS_ADMIN), access to /dev/fuse on the host machine (--device /dev/fuse), and have the PID space shared with the host machine (--pid host).
# WARNING: Given the shared PID space, running this container on a host where eos daemons are already
#			in execution may interfere with their normal operation.
# 
#	-->	To run the container WITHOUT EOS access from the outside of the container: 
#			1. docker build -t eos -f eos.Dockerfile .
#			2. docker run --name eos_mount --cap-add SYS_ADMIN --device /dev/fuse --pid host -t eos
#
#	-->	To run the container WITH EOS access from the outside of the container: 
#			1. mkdir -p /deploy_docker/eos_mount
#				==> MAKE SURE THE DIRECTORY IS EMPTY! <==
#			2. docker build -t eos -f eos.Dockerfile .
#			3. docker run --name eos_mount --cap-add SYS_ADMIN --device /dev/fuse --pid host --volume /deploy_docker/eos_mount:/eos:shared -t eos
#		-->	If you then need to access EOS from another docker container, 
#			run the latter as, e.g., :
#			docker run -it --volume /deploy_docker/eos_mount:/eos ubuntu bash
#		--> To remove the /deploy_docker/eos_mount subfolders when the EOS container is gone run
#				fusermount -u /deploy_docker/eos_mount/<folder_to_be_removed>, or
#				sudo umount -l /deploy_docker/eos_mount/<folder_to_be_removed>
#			and the proceed with the typical rmdir <folder_to_be_removed>
#

#
# Build and Run for testing purposes:
# docker build -t eos-fusex .
#
# CentOS 7:
#   docker run --rm --device /dev/fuse --cap-add SYS_ADMIN -it eos-fusex bash
# Ubuntu 18.04:
#   docker run --rm --device /dev/fuse --cap-add SYS_ADMIN --security-opt apparmor:unconfined -it eos-fusex bash
#


FROM gitlab-registry.cern.ch/sciencebox/docker-images/parent-images/base:v0

MAINTAINER Enrico Bocchi <enrico.bocchi@cern.ch>


# ----- Supervisord ----- #
RUN yum -y install \
      supervisor && \
    yum clean all && \
    rm -rf /var/cache/yum


# ----- SSSD ----- #
RUN yum -y install \
      sssd && \
    yum clean all && \
    rm -rf /var/cache/yum
ADD ./sssd.d/sssd.conf /etc/sssd/sssd.conf
RUN chmod 600 /etc/sssd/sssd.conf


# ----- EOS FUSEx ----- #
ADD ./yum.repos.d/ /etc/yum.repos.d/
RUN yum -y install \
      jemalloc && \
    yum clean all && \
    rm -rf /var/cache/yum

ARG EOSXD_VERSION 
RUN yum -y install \
      eos-client$EOSXD_VERSION \
      eos-fusex$EOSXD_VERSION \
      eos-fusex-core$EOSXD_VERSION \
      eos-fusex-selinux$EOSXD_VERSION && \
    yum clean all && \
    rm -rf /var/cache/yum

#RUN yum -y install \
#      eos-debuginfo && \
#    yum clean all && \
#    rm -rf /var/cache/yum

# ----- EOS FUSEx config files and keytab ----- #
RUN rm -f /etc/eos/*.conf
ADD ./eosxd.d/fuse.openup2u.conf /etc/eos/fuse.openup2u.conf
#ADD ./eosxd.d/fuse.sss.keytab /etc/eos/fuse.sss.keytab
#RUN chmod 600 /etc/eos/fuse.sss.keytab
RUN mkdir /eos

# ----- Add supervisord configuration files for installed daemons ----- #
ADD ./supervisord.d/kill_supervisor.py /etc/supervisord.d/kill_supervisor.py
ADD ./supervisord.d/kill_supervisor.ini /etc/supervisord.d/kill_supervisor.ini
#ADD ./supervisord.d/crond.ini /etc/supervisord.d/crond.noload
ADD ./supervisord.d/sssd.ini /etc/supervisord.d/sssd.ini
ADD ./supervisord.d/supervisord.conf /etc/supervisord.conf


## ----- Copy the gateway keytab and set the correct permissions ----- #
#ADD ./eos-storage.citrine.d/utils/configure_gateway.sh /root/configure_gateway.sh
#ADD ./eos-storage.citrine.d/keytabs/eos-gateway.keytab /etc/eos-gateway.keytab
#RUN chown daemon:daemon /etc/eos-gateway.keytab
#RUN chmod 600 /etc/eos-gateway.keytab
#
#
# ----- Run the setup script in the container ----- #
ADD ./eosxd.d/start.sh /root/start.sh 
ADD ./eosxd.d/stop.sh /root/stop.sh
CMD ["/bin/bash", "/root/start.sh"]

